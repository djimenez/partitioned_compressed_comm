# MPI Partitioned Communication with ZFP compression

This repository is a simple test of MPI Partitioned Communication with compressed buffers. 
A linearized 6D array is partitioned into multiple 3D sub-blocks which are compressed with ZFP and 
sent from one rank to another rank. The receiving rank receives each partition and decompresses into
a global array again. 

This code has been tested on Alex (NHR@FAU).

1. Load required modules in Alex

-  gcc/12.1.0 cmake/3.23.1 cuda/12.3.0

2. Clone the zfp repository: https://github.com/LLNL/zfp.git

3. cd zfp && mkdir build && cd build

4. cmake  -DBUILD_CFP=ON -DBUILD_ZFORP=ON -DBUILD_TESTING=ON -DBUILD_SHARED_LIBS=ON -DZFP_WITH_OPENMP=ON -DZFP_WITH_CUDA=ON -DZFP_BIT_STREAM_WORD_SIZE=64 ..

5. cd ~ 

6. On Alex, you need to install OpenMPI 5 through Spack:

- module load user-spack

- spack install openmpi+cuda@5.0.3 ^pmix@5.0.1

7. After installing OpenMPI with Spack you need to load the user module from Spack. 

- module av openmpi
- You should see the name of the module installed by Spack.
- Example: module load openmpi/5.0.3-gcc12.1.0-cuda-5odvvmf

8. Now go back to the partitioned_compressed_comm directory. 

- mkdir build && cd build
- cmake .. -DZFP_DIR=<path/to/zfp/build> 
- make

