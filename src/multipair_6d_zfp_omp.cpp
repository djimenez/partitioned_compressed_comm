#include <iostream>
#include <cstring>
#include <sstream>
#include "zfp_compression.h"
#include "mpi.h"
#include <random>
#include <chrono>


void initializeArrayHost6D(double* array, int nx0, int nky0, int nz0, int nv0, int nw0, int n_spec) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> dis(0.0, 1.0);

    for (int l = 0; l < n_spec; ++l) {
        for (int k = 0; k < nw0; ++k) {
            for (int j = 0; j < nv0; ++j) {
                for (int i = 0; i < nz0; ++i) {
                    for (int h = 0; h < nky0; ++h) {
                        for (int g = 0; g < nx0; ++g) {
                            // Calculate the linear index using Fortran ordering
                            int index = g + nx0 * (h + nky0 * (i + nz0 * (j + nv0 * (k + nw0 * l))));

                            // Initialize the element
                            array[index] = dis(gen);
                        }
                    }
                }
            }
        }
    }
}

void writeArrayToFile(double* array, int nx0, int nky0, int nz0, int nv0, int nw0, int n_spec, const std::string& filename) {
    std::ofstream outputFile(filename); 
    if (outputFile.is_open()) {
        for (int l = 0; l < n_spec; ++l) {
            for (int k = 0; k < nw0; ++k) {
                for (int j = 0; j < nv0; ++j) {
                    for (int i = 0; i < nz0; ++i) {
                        for (int h = 0; h < nky0; ++h) {
                            for (int g = 0; g < nx0; ++g) {
                                // Calculate the linear index using Fortran ordering
                                int index = g + nx0 * (h + nky0 * (i + nz0 * (j + nv0 * (k + nw0 * l))));
                                // Write the value to the file
                                outputFile << array[index] << " ";
                            }
                            outputFile << std::endl;
                        }
                        outputFile << std::endl;
                    }
                    outputFile << std::endl;
                }
                outputFile << std::endl;
            }
            outputFile << std::endl;
        }
        outputFile.close();
        std::cout << "Array written to file '" << filename << "'." << std::endl;
    } else {
        std::cout << "Unable to open file '" << filename << "'." << std::endl;
    }
}

int main(int argc, char *argv[]){
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    if(provided < MPI_THREAD_MULTIPLE){
    	MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    }
    int nx0 = 96; 
    int nky0 = 16; 
    int nz0 = 20; 
    int nv0 = 2; 
    int nw0 = 8; 
    int n_spec = 1; 
    int* n_dimensions = new int(3);
    n_dimensions[0] = nx0; 
    n_dimensions[1] = nky0; 
    n_dimensions[2] = nz0;

    size_t compression_buffer_size, actual_compressed_size;
    int compress_rate = 64;
    int totalSize = nx0 * nky0 * nz0 * nv0 * nw0 * n_spec;

    int my_rank, size;
    int tag = 1, flag = 0;
    MPI_Request request;
    int device_id;
    int num_devices;


    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);
    std::string processor_name_str(processor_name, name_len);
    int couple = (my_rank+size/2)%size;
    int is_first_half_rank = (my_rank < size/2) ? 0 : MPI_UNDEFINED;
    MPI_Comm first_half_ranks;
    MPI_Comm_split(MPI_COMM_WORLD, is_first_half_rank, my_rank, &first_half_ranks);

    cudaGetDeviceCount(&num_devices);
    cudaSetDevice(my_rank%num_devices);
    cudaGetDevice(&device_id);
    std::cout << "Rank "<< my_rank <<"Device ID: " << device_id << " on node: " << processor_name_str << std::endl;

    double* h_array = new double[totalSize];

    if(is_first_half_rank == 0){
        std::cout << "OPENMP EXECUTION" << std::endl;
        std::cout << "Initializing data on host....." << std::endl;
        initializeArrayHost6D(h_array, nx0, nky0, nz0, nv0, nw0, n_spec);
        std::cout << "Initialization complete" << std::endl;
    }

    double* d_array_original;
    cudaMalloc((void**)&d_array_original, totalSize * sizeof(double));
    
    if(is_first_half_rank == 0){
	std::ostringstream filename;
    	filename << "omp_initial_array_" << my_rank << ".txt";
        writeArrayToFile(h_array, nx0, nky0, nz0, nv0, nw0, n_spec, filename.str());
        cudaMemcpy(d_array_original, h_array, totalSize * sizeof(double), cudaMemcpyHostToDevice);
    }

    MPI_Count partitions = n_spec * nw0 * nv0;
    int subblockSize = nx0 * nky0 * nz0;
    double partition_size = (subblockSize*sizeof(double))/((sizeof(double)*8)/compress_rate);

    unsigned char* compressed_message;
    ZFPCompressor<double> estimator(&d_array_original[0], 3, n_dimensions, compress_rate, GPU_BACKEND);
    size_t estimated_block_size = estimator.estimate_compressed_buffer_size();
    cudaMalloc((void**)&compressed_message, n_spec * nw0 * nv0 * estimated_block_size * sizeof(unsigned char));
    
    double total_time = 0.0;
    double compression_time = 0.0;
    double decompression_time = 0.0;
    int iterations = 200;
    std::chrono::time_point<std::chrono::high_resolution_clock> iteration_start;

    for(int i=0; i<iterations; i++){
    	MPI_Barrier(MPI_COMM_WORLD);
	if(my_rank==0){
            iteration_start = std::chrono::high_resolution_clock::now();
        }

    	if(is_first_half_rank == 0){
    	    std::cout << "Rank " << my_rank << "compressing and sending" << std::endl;
    	    MPI_Psend_init(compressed_message, partitions, partition_size, MPI_BYTE, couple, tag, MPI_COMM_WORLD, MPI_INFO_NULL, &request);
    	    MPI_Start(&request);
	    #pragma omp parallel for shared(request)
    	    for(int i=0; i< n_spec*nw0*nv0; i++){
    	            // Calculate the starting index of the current subblock
    	            int startIdx = (i * subblockSize);
    	            //std::cout << "Index = " << startIdx << std::endl;
    	            double* subblock = &d_array_original[startIdx];
    	            ZFPCompressor<double> zfp_compressor(subblock, 3, n_dimensions, compress_rate, GPU_BACKEND);
    	            int startIdx_compressed = (i * partition_size);
    	            //std::cout << "compressed index: " << startIdx_compressed << std::endl;
    	            unsigned char* compressed_subblock = compressed_message +startIdx_compressed; 
    	            zfp_compressor.compress_fixed_rate(compressed_subblock, estimated_block_size);
    	            MPI_Pready(i, request);

    	            //actual_compressed_size = zfp_compressor.get_compressed_size();
    	            //std::cout << "Actual compressed size is: " << actual_compressed_size << std::endl;
    	            std::cout << "[ "<<my_rank<<"]  Done compressing subblock: " << i << std::endl;
    	    }
    	    while(!flag){MPI_Test(&request, &flag, MPI_STATUS_IGNORE);}
    	    MPI_Request_free(&request);
    	}

    	if(is_first_half_rank != 0){
    	    std::cout << "Rank " << my_rank << "receiving and decompressing" << std::endl;
    	    MPI_Precv_init(compressed_message, partitions, partition_size, MPI_BYTE, couple, tag, MPI_COMM_WORLD, MPI_INFO_NULL, &request);
    	    MPI_Start(&request);
	    #pragma omp parallel for shared(request)
    	    for(int j=0; j< n_spec*nw0*nv0; j++){
    	        int part_flag = 0;
    	        while(!part_flag){
    	            MPI_Parrived(request, j, &part_flag);
    	        }
    	        int startIdx = (j * subblockSize);
    	        double* subblock = &d_array_original[startIdx];
    	        ZFPCompressor<double> zfp_decompressor(subblock, 3, n_dimensions, compress_rate, GPU_BACKEND);
    	        int startIdx_compressed = (j * partition_size);
    	        zfp_decompressor.decompress_fixed_rate(&compressed_message[startIdx_compressed], estimated_block_size);
    	        std::cout << "[ "<<my_rank<<"]  Done decompressing subblock: " << j << std::endl;
    	    }
    	    while(!flag){MPI_Test(&request, &flag, MPI_STATUS_IGNORE);}
    	    MPI_Request_free(&request);

    	}
	
	MPI_Barrier(MPI_COMM_WORLD);
        if(my_rank==0){
            auto iteration_end = std::chrono::high_resolution_clock::now();
            double iteration_duration = std::chrono::duration<double>(iteration_end - iteration_start).count();
            if(i>=100){
                total_time += iteration_duration;
            }
        }
    }

    if(is_first_half_rank != 0){
    	std::ostringstream filename;
    	filename << "omp_decompressed_array_" << my_rank << ".txt";
    	cudaMemcpy(h_array, d_array_original, totalSize * sizeof(double), cudaMemcpyDeviceToHost);
    	writeArrayToFile(h_array, nx0, nky0, nz0, nv0, nw0, n_spec, filename.str()); 
    }
    if(my_rank==0){
        double avg_total_time = total_time / 100;
        std::cout << "\nAverage iteration time: " << avg_total_time << " seconds" << std::endl;
    }


    // Free memory on host and device
    delete[] h_array;
    cudaFree(d_array_original);
    cudaFree(compressed_message);

    MPI_Finalize();
    return 0;
}
