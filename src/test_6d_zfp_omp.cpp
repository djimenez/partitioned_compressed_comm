#include <iostream>
#include "zfp_compression.h"
#include "mpi.h"
#include <random>


void initializeArrayHost6D(double* array, int nx0, int nky0, int nz0, int nv0, int nw0, int n_spec) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> dis(0.0, 1.0);

    for (int l = 0; l < n_spec; ++l) {
        for (int k = 0; k < nw0; ++k) {
            for (int j = 0; j < nv0; ++j) {
                for (int i = 0; i < nz0; ++i) {
                    for (int h = 0; h < nky0; ++h) {
                        for (int g = 0; g < nx0; ++g) {
                            // Calculate the linear index using Fortran ordering
                            int index = g + nx0 * (h + nky0 * (i + nz0 * (j + nv0 * (k + nw0 * l))));

                            // Initialize the element
                            array[index] = dis(gen);
                        }
                    }
                }
            }
        }
    }
}

void writeArrayToFile(double* array, int nx0, int nky0, int nz0, int nv0, int nw0, int n_spec, const std::string& filename) {
    std::ofstream outputFile(filename); 
    if (outputFile.is_open()) {
        for (int l = 0; l < n_spec; ++l) {
            for (int k = 0; k < nw0; ++k) {
                for (int j = 0; j < nv0; ++j) {
                    for (int i = 0; i < nz0; ++i) {
                        for (int h = 0; h < nky0; ++h) {
                            for (int g = 0; g < nx0; ++g) {
                                // Calculate the linear index using Fortran ordering
                                int index = g + nx0 * (h + nky0 * (i + nz0 * (j + nv0 * (k + nw0 * l))));
                                // Write the value to the file
                                outputFile << array[index] << " ";
                            }
                            outputFile << std::endl;
                        }
                        outputFile << std::endl;
                    }
                    outputFile << std::endl;
                }
                outputFile << std::endl;
            }
            outputFile << std::endl;
        }
        outputFile.close();
        std::cout << "Array written to file '" << filename << "'." << std::endl;
    } else {
        std::cout << "Unable to open file '" << filename << "'." << std::endl;
    }
}

int main(int argc, char *argv[]){
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    if(provided < MPI_THREAD_MULTIPLE){
    	MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    }
    int nx0 = 96; 
    int nky0 = 16; 
    int nz0 = 20; 
    int nv0 = 2; 
    int nw0 = 8; 
    int n_spec = 1; 
    int* n_dimensions = new int(3);
    n_dimensions[0] = nx0; 
    n_dimensions[1] = nky0; 
    n_dimensions[2] = nz0;

    size_t compression_buffer_size, actual_compressed_size;
    int compress_rate = 32;
    int totalSize = nx0 * nky0 * nz0 * nv0 * nw0 * n_spec;

    int my_rank;
    int source = 0, dest = 1, tag = 1, flag = 0;
    MPI_Request request;

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    double* h_array = new double[totalSize];

    if(my_rank == 0){
        std::cout << "Initializing data on host....." << std::endl;
        initializeArrayHost6D(h_array, nx0, nky0, nz0, nv0, nw0, n_spec);
        std::cout << "Initialization complete" << std::endl;
        writeArrayToFile(h_array, nx0, nky0, nz0, nv0, nw0, n_spec, "omp_initial_array.txt");
    }

    double* d_array_original;
    cudaMalloc((void**)&d_array_original, totalSize * sizeof(double));
    
    if(my_rank == 0){
        std::cout << "Copying data to device....." << std::endl;
        cudaMemcpy(d_array_original, h_array, totalSize * sizeof(double), cudaMemcpyHostToDevice);
        std::cout << "Copying done" << std::endl;
    }

    MPI_Count partitions = n_spec * nw0 * nv0;
    int subblockSize = nx0 * nky0 * nz0;
    double partition_size = (subblockSize*sizeof(double))/((sizeof(double)*8)/compress_rate);

    unsigned char* compressed_message;
    ZFPCompressor<double> estimator(&d_array_original[0], 3, n_dimensions, compress_rate, GPU_BACKEND);
    size_t estimated_block_size = estimator.estimate_compressed_buffer_size();
    cudaMalloc((void**)&compressed_message, n_spec * nw0 * nv0 * estimated_block_size * sizeof(unsigned char));


    if(my_rank == 0){
        MPI_Psend_init(compressed_message, partitions, partition_size, MPI_BYTE, dest, tag, MPI_COMM_WORLD, MPI_INFO_NULL, &request);
        MPI_Start(&request);
	#pragma omp parallel for shared(request)
        for(int i=0; i< n_spec*nw0*nv0; i++){
                // Calculate the starting index of the current subblock
                int startIdx = (i * subblockSize);
                std::cout << "Index = " << startIdx << std::endl;
                double* subblock = &d_array_original[startIdx];
                ZFPCompressor<double> zfp_compressor(subblock, 3, n_dimensions, compress_rate, GPU_BACKEND);
                int startIdx_compressed = (i * partition_size);
                std::cout << "compressed index: " << startIdx_compressed << std::endl;
                unsigned char* compressed_subblock = compressed_message +startIdx_compressed; 
                zfp_compressor.compress_fixed_rate(compressed_subblock, estimated_block_size);
                MPI_Pready(i, request);

                actual_compressed_size = zfp_compressor.get_compressed_size();
                std::cout << "Actual compressed size is: " << actual_compressed_size << std::endl;
                std::cout << "Done compressing subblock: " << startIdx << std::endl;
        }
        while(!flag){MPI_Test(&request, &flag, MPI_STATUS_IGNORE);}
        MPI_Request_free(&request);
    }

    if(my_rank == 1){
        MPI_Precv_init(compressed_message, partitions, partition_size, MPI_BYTE, source, tag, MPI_COMM_WORLD, MPI_INFO_NULL, &request);
        MPI_Start(&request);
	#pragma omp parallel for shared(request)
        for(int j=0; j< n_spec*nw0*nv0; j++){
            int part_flag = 0;
            while(!part_flag){
                MPI_Parrived(request, j, &part_flag);
            }
            int startIdx = (j * subblockSize);
            double* subblock = &d_array_original[startIdx];
            ZFPCompressor<double> zfp_decompressor(subblock, 3, n_dimensions, compress_rate, GPU_BACKEND);
            int startIdx_compressed = (j * partition_size);
            zfp_decompressor.decompress_fixed_rate(&compressed_message[startIdx_compressed], estimated_block_size);
        }
        while(!flag){MPI_Test(&request, &flag, MPI_STATUS_IGNORE);}
        MPI_Request_free(&request);

        std::cout << "Rank 1 Copying data from device to host....." << std::endl;
        cudaMemcpy(h_array, d_array_original, totalSize * sizeof(double), cudaMemcpyDeviceToHost);
        std::cout << "Rank 1 Writing data to stdio" << std::endl;
        writeArrayToFile(h_array, nx0, nky0, nz0, nv0, nw0, n_spec, "omp_decompressed_array.txt"); 
    }

    // Free memory on host and device
    delete[] h_array;
    cudaFree(d_array_original);
    cudaFree(compressed_message);

    MPI_Finalize();
    return 0;
}
